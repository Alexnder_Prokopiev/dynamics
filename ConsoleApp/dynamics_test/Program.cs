﻿using System;
using dynamics_test.Implemented;
using dynamics_test.Menu;

namespace dynamics_test
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            BookRepository br = new BookRepository(new FileManager());

            br.Create(new Book { Title = "Title3", Author = "Author3", Descrition = "Descrition3" });
            br.Create(new Book { Title = "Title", Author = "Author", Descrition = "Descrition" });
            br.Create(new Book { Title = "Title1", Author = "Author1", Descrition = "Descrition1" });
            br.Create(new Book { Title = "Title2", Author = "Author2", Descrition = "Descrition2" });

            Menu<Book> menu = new Menu<Book>(br);
            var binder = new ConsoleBookBinder();

            menu.Launch(binder);

            Console.Read();
        }      
    }   
}
