﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using dynamics_test.Interfaces;
using System.IO;
using System;

namespace dynamics_test
{
    public class BookRepository : IRepository<Book>
    {
        private List<Book> _books = new List<Book>();//
        private readonly IFileManager<Book> _fileManager;//

        public BookRepository(IFileManager<Book> fileManager)//sdf123123
        {
            _fileManager = fileManager;
        }

        public int Count
        {
            get
            {
                return _books.Count;
            }
        }

        public List<Book> GetAll
        {
            get
            {
                return _books;
            }
        }

        public void Create(Book book)
        {
            if(book == null)
            {
                throw new NullReferenceException();
            }

            _books.Add(book);
        }

        public Book GetBook(int id)
        {
            return _books[id];
        }

        public void Delete(int index)
        {
            _books.RemoveAt(index);
        }

        public void Update(int id, Book newBook)
        {
            _books[id] = newBook;
        }

        public IEnumerable<Book> Find(Func<Book, bool> search)
            => _books.Where(search);

        public void Sort(Func<Book, string> sort, bool reversed) 
        {
            _books = reversed == false ? _books.OrderBy(sort).ToList() : _books.OrderByDescending(sort).ToList();
        }

        public void SaveToFileJson(Stream dialogPath)
        {
            string input = JsonConvert.SerializeObject(_books);

            _fileManager.SaveToFile(input, dialogPath);
        }

        public IEnumerable<Book> LoadFromFileJson(Stream dialogPath)
        {
            string text = _fileManager.LoadFromFile(dialogPath);

            return JsonConvert.DeserializeObject<IReadOnlyCollection<Book>>(text);
        }

        public IEnumerable<Book> GetPages(int skipPages, int pageLength)
            => _books.Skip(skipPages * pageLength)
                     .Take(pageLength);
    }
}
