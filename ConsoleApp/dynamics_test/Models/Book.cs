﻿namespace dynamics_test
{
    public class Book
    {
        public string Title { get; set; }

        public string Author { get; set; }

        public string Descrition { get; set; }

        public override string ToString()
        {
            return $"{Title} {Author} {Descrition}";
        }
    }
}
