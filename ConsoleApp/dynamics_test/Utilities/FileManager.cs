﻿using dynamics_test.Interfaces;
using System.IO;

namespace dynamics_test.Implemented
{
    public class FileManager : IFileManager<Book>
    {
        public string LoadFromFile(Stream dialogPath)
        {
            string text = null;

            using (StreamReader sr = new StreamReader(dialogPath))
            {
                text = sr.ReadToEnd();
            }

            return text;
        }

        public void SaveToFile(string input, Stream dialogPath)
        {
            using (StreamWriter writer = new StreamWriter(dialogPath))
            {
                writer.WriteLine(input);
            }
        }
    }
}
