﻿using dynamics_test.Interfaces;
using System;

namespace dynamics_test.Implemented
{
    class ConsoleBookBinder : IDataBinder<Book>
    {
        public Book BindModel()
        {
            Console.WriteLine("Enter book title:");
            var title = Console.ReadLine();
            Console.WriteLine("Enter book author:");
            var author = Console.ReadLine();
            Console.WriteLine("Enter book descritpion:");
            var descritpion = Console.ReadLine();
            
            return new Book { Title = title, Author = author, Descrition = descritpion };
        }

        public int GetId()
        {
            Console.WriteLine("Enter number:");

            int.TryParse(Console.ReadLine(), out int id);

            return id;
        }

        public bool IsValid(Book book)
        {
            if(string.IsNullOrEmpty(book.Title) || string.IsNullOrEmpty(book.Author))
            {
                return false;
            }

            return true;
        }
    }
}
