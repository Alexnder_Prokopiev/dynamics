﻿using dynamics_test.Interfaces;
using System;
using System.Windows.Forms;

namespace dynamics_test.Implemented
{
    public class Create<T>: IStrategy<T>
    {
        public IDataBinder<T> Binder { get; }

        public IRepository<T> Repository { get; }

        public Create(IDataBinder<T> binder, IRepository<T> repository)
        {
            Binder = binder;
            Repository = repository;
        }

        public void Execute()
        {
            T model = Binder.BindModel();
            if(Binder.IsValid(model))
            {
                Repository.Create(model);
            }
            else
            {
                Console.WriteLine("Model is invalid.");
            }
        }
    }

    public class GetItem<T> : IStrategy<T>
    {
        public IDataBinder<T> Binder { get; }

        public IRepository<T> Repository { get; }

        public GetItem(IDataBinder<T> binder, IRepository<T> repository)
        {
            Binder = binder;
            Repository = repository;
        }

        public void Execute()
        {
            var id = Binder.GetId();

            if(id <= Repository.Count)
            {
                Console.WriteLine(Repository.GetBook(id).ToString());
            }
        }
    }

    public class Delete<T> : IStrategy<T>
    {
        public IDataBinder<T> Binder { get; }

        public IRepository<T> Repository { get; }

        public Delete(IDataBinder<T> binder, IRepository<T> repository)
        {
            Binder = binder;
            Repository = repository;
        }

        public void Execute()
        {
            var id = Binder.GetId();

            if (id <= Repository.Count)
            {
                Repository.Delete(id);
            }
            else
            {
                Console.WriteLine("Incorrect id.");
            }
        }
    }

    public class Update<T> : IStrategy<T>
    {
        public IDataBinder<T> Binder { get; }

        public IRepository<T> Repository { get; }

        public Update(IDataBinder<T> binder, IRepository<T> repository)
        {
            Binder = binder;
            Repository = repository;
        }

        public void Execute()
        {
            var id = Binder.GetId();

            if (id <= Repository.Count)
            {
                T model = Binder.BindModel();

                if (Binder.IsValid(model))
                {
                    Repository.Create(model);
                }
                else
                {
                    Console.WriteLine("Model is invalid.");
                }
            }
            else
            {
                Console.WriteLine("Incorrect id.");
            }
        }
    }

    public class SaveToFileJson<T> : IStrategy<T>
    {
        public IRepository<T> Repository { get; }

        public SaveToFileJson(IRepository<T> repository)
        {
            Repository = repository;
        }

        public void Execute()
        {
            var saveDialog = new SaveFileDialog
            {
                FileName = "Bookstorage.txt",
                Filter = "Text File | *.txt"
            };

            if (saveDialog.ShowDialog() == DialogResult.OK)
            {
                Repository.SaveToFileJson(saveDialog.OpenFile());
            }
        }
    }

    //clarify its purpose
    public class LoadFromFileJson<T> : IStrategy<T>
    {
        public IRepository<T> Repository { get; }

        public LoadFromFileJson(IRepository<T> repository)
        {
            Repository = repository;
        }

        public void Execute()
        {
            var openDialog = new OpenFileDialog
            {
                FileName = "Bookstorage.txt",
                Filter = "Text File | *.txt"
            };

            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                //TODO : implement stuff according to whatever its purpose is
                var output = Repository.LoadFromFileJson(openDialog.OpenFile());
            }
        }
    }

    public class GetPages<T> : IStrategy<T>
    {
        public IDataBinder<T> Binder { get; }

        public IRepository<T> Repository { get; }

        public GetPages(IDataBinder<T> binder, IRepository<T> repository)
        {
            Binder = binder;
            Repository = repository;
        }

        public void Execute()
        {
            var skip = Binder.GetId();
            var pages = Binder.GetId();

            foreach(var book in Repository.GetPages(skip, pages))
            {
                Console.WriteLine(book.ToString());
            }
            Console.WriteLine();
        }
    }

    public class Sort<T> : IStrategy<T>
    {
        public IRepository<T> Repository { get; }

        public void Execute()
        {
            throw new NotImplementedException();
        }
    }
}
