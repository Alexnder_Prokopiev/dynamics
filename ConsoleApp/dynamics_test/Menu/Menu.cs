﻿using dynamics_test.Implemented;
using dynamics_test.Interfaces;
using System;
using System.Collections.Generic;

namespace dynamics_test.Menu
{
    class Menu<T>
    {
        public Dictionary<int, IStrategy<T>> Commands { get; }
        private IRepository<T> _repository;

        public Menu(IRepository<T> repository)
        {
            _repository = repository;
            Commands = new Dictionary<int, IStrategy<T>>();
        }

        public void Launch(IDataBinder<T> binder)
        {
            Commands.Add(1, new Create<T>(binder, _repository));
            Commands.Add(2, new GetItem<T>(binder, _repository));
            Commands.Add(3, new Delete<T>(binder, _repository));
            Commands.Add(4, new Update<T>(binder, _repository));
            Commands.Add(5, new SaveToFileJson<T>(_repository));
            Commands.Add(6, new LoadFromFileJson<T>(_repository));
            Commands.Add(7, new GetPages<T>(binder, _repository));

            bool stop = false;

            do
            {
                Console.WriteLine("Choose option:");
                Console.WriteLine("1, create item.");
                Console.WriteLine("2, get item.");
                Console.WriteLine("3, delete item.");
                Console.WriteLine("4, update item.");
                Console.WriteLine("5, SaveToFileJson item.");
                Console.WriteLine("6, LoadFromFileJson item.");
                Console.WriteLine("7, GetPages item.\n");

                int.TryParse(Console.ReadLine(), out int input);

                if (input == 0)
                {
                    stop = true;
                }
                else
                {
                    Commands[input].Execute();
                }
            }
            while (stop);
        }
    }
}
