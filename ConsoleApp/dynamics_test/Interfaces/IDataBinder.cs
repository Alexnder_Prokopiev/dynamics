﻿namespace dynamics_test.Interfaces
{
    public interface IDataBinder<T>
    {
        T BindModel();

        int GetId();

        bool IsValid(T book);
    }
}
