﻿namespace dynamics_test.Interfaces
{
    public interface IStrategy<T>
    {
        IRepository<T> Repository { get; }

        void Execute();
    }
}
