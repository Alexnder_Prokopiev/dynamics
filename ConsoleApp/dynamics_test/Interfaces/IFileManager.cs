﻿using System.IO;

namespace dynamics_test.Interfaces
{
    public interface IFileManager<T>
    {
        void SaveToFile(string input, Stream dialogPath);

        string LoadFromFile(Stream dialogPath);
    }
}
