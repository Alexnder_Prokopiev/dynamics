﻿using System;
using System.Collections.Generic;
using System.IO;

namespace dynamics_test.Interfaces
{
    public interface IRepository<T>
    {
        List<T> GetAll { get; }
        void Create(T item);

        Book GetBook(int id);

        void Delete(int index);

        void Update(int id, T newItem);

        int Count { get; }

        void Sort(Func<T, string> sort, bool reversed);

        IEnumerable<Book> Find(Func<T, bool> search);

        IEnumerable<Book> LoadFromFileJson(Stream dialogPath);

        void SaveToFileJson(Stream dialogPath);

        IEnumerable<Book> GetPages(int skipPages, int pageLength);
    }
}
